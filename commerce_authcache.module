<?php

/**
 * @info
 * Defines the Commerce Authcache module implementation.
 *
 * The Commerce Authcache module provides Authcache integration for the Commerce module.
 */

/**
 * Implements hook_boot().
 */
function commerce_authcache_boot() {
  // Dummy implementation - allows the .module file to be included at bootstrap.
  // This allows us to write the _authcache callbacks in our module file, instead
  // of copying them to sites/*/authcache_custom.php.
}

/**
 * Implements hook_block_view_MODULE_DELTA_alter() for the Commerce Cart cart block.
 */
function commerce_authcache_block_view_commerce_cart_cart_alter(&$data, $block) {
  global $_authcache_is_cacheable;

  if ($_authcache_is_cacheable) {
    commerce_authcache_add_js();
    $data['content'] = '<div id="commerce-authcache--commerce-cart-cart-block" class="authcache-target commerce-authcache-target">&nbsp;</div>';
  }
}

/**
 * Includes the module JS.
 */
function commerce_authcache_add_js() {
  drupal_add_js(drupal_get_path('module', 'commerce_authcache') .'/js/commerce_authcache.js');
}

/**
 * Ajax callback for rendering the Commerce Cart cart block content.
 */
function _authcache_commerce_authcache__commerce_cart_cart_block() {
  // We need to bootstrap Drupal fully, as we need the theme system, form API, unicode handling as well as all module
  // dependencies fully loaded.
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

  return commerce_cart_block_view('cart');
}