/**
 * @file
 * Defines the AJAX callbacks used by Authcache, as well as the Drupal.behaviors definition.
 */

;(function($, Drupal, undefined) {

  Drupal.behaviors.commerceAuthcache = {

    attach: function(context) {
      // Cart block.
      if ($('#commerce-authcache--commerce-cart-cart-block', context).length) {
        Authcache.ajaxRequest({
          commerce_authcache__commerce_cart_cart_block: 123,
        });
      }
    }

  };

})(jQuery, Drupal);

/**
 * Authcache AJAX callback function for updating the Commerce Cart block.
 */
function _authcache_commerce_authcache__commerce_cart_cart_block(vars) {
  jQuery('#commerce-authcache--commerce-cart-cart-block').replaceWith(vars.content);
}